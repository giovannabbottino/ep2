package ep2;

public class Moto extends Veiculo{

	public Moto() {
		this.setCombustivel("gasolina");
		this.setCargaMaxima(50);
		this.setVelocidade(110);
		this.setTipo("Moto");
		this.setStatus("Disponivel");
	}
	
	public void setCombustivel(String gas) {
		switch (gas) {
        case "alcool":
            this.setDefaultRendimento(43);
            this.setDiminuiRendimento(0.4);
            this.setValorCombustivel(3.499);
            break;
        case "gasolina":
            this.setDefaultRendimento(50);
            this.setDiminuiRendimento(0.3);
            this.setValorCombustivel(4.449);
            break;
        default:
             System.out.println("Este não é um combustivel válido!");
     }
		this.combustivel = gas;
		
	}
}
