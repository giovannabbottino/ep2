package ep2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class Menu extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textPeso;
	private JTextField textDistancia;
	private JTextField textPrazo;
	private double peso;
	private double distancia;
	private double prazo;
	Vector<Veiculo> veiculos  = new Vector<Veiculo>();
	Vector<Veiculo> v1 = new Vector<Veiculo>();
	
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 550, 300);
	}
	
	public void Inicial() {
		/***************************** Inicialização do Panel ***********************************/
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{530, 0};
		gbl_contentPane.rowHeights = new int[]{31, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		/***************************** Criação da primeira ToolBar ***********************************/
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		GridBagConstraints gbc_toolBar = new GridBagConstraints();
		gbc_toolBar.insets = new Insets(0, 0, 5, 0);
		gbc_toolBar.anchor = GridBagConstraints.NORTH;
		gbc_toolBar.fill = GridBagConstraints.HORIZONTAL;
		gbc_toolBar.gridx = 0;
		gbc_toolBar.gridy = 0;
		contentPane.add(toolBar, gbc_toolBar);		
		/***************************** Botão de novo veiculo - (+) ***********************************/
		JButton btnVeiculo = new JButton("+");
		btnVeiculo.setToolTipText("Novo Veiculo");
		toolBar.add(btnVeiculo);
		btnVeiculo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  
				contentPane.setVisible(false);
				NovoVeiculo();			    
			}
		});
		/***************************** FIM DO Botão de novo veiculo - (+) ***********************************/
		JTextPane txtpnPeso = new JTextPane();
		toolBar.add(txtpnPeso);
		txtpnPeso.setBackground(UIManager.getColor("ToolBar.background"));
		txtpnPeso.setEditable(false);
		txtpnPeso.setText("Peso");
		
		textPeso = new JTextField();
		textPeso.setToolTipText("kg");
		toolBar.add(textPeso);
		textPeso.setDropMode(DropMode.INSERT);
		textPeso.setBackground(Color.WHITE);
		textPeso.setColumns(10);
		
		JTextPane txtpnDistancia = new JTextPane();
		toolBar.add(txtpnDistancia);
		txtpnDistancia.setBackground(UIManager.getColor("ToolBar.background"));
		txtpnDistancia.setEditable(false);
		txtpnDistancia.setText("Distância");
		
		textDistancia = new JTextField();
		textDistancia.setToolTipText("km");
		toolBar.add(textDistancia);
		textDistancia.setDropMode(DropMode.INSERT);
		textDistancia.setBackground(Color.WHITE);
		textDistancia.setColumns(10);
		
		JTextPane txtpnPrazo = new JTextPane();
		toolBar.add(txtpnPrazo);
		txtpnPrazo.setBackground(UIManager.getColor("ToolBar.background"));
		txtpnPrazo.setEditable(false);
		txtpnPrazo.setText("Prazo");
		
		textPrazo = new JTextField();
		textPrazo.setToolTipText("horas");
		toolBar.add(textPrazo);
		textPrazo.setDropMode(DropMode.INSERT);
		textPrazo.setBackground(Color.WHITE);
		textPrazo.setColumns(10);
		
		JButton btnNovaViagem = new JButton("Nova Viagem");
		toolBar.add(btnNovaViagem);
		
		String[] a = {"","Custo Beneficio","+ rápido","- custo"};
		JComboBox<String> comboBox = new JComboBox<String>(a);
		comboBox.setToolTipText("Filtro");
		toolBar.add(comboBox);
		comboBox.setEnabled(false);
		/***************************** FIM da primeira ToolBar ***********************************/
		/***************************** Criação do srool Pane ***********************************/
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 50, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		scrollPane.setVisible(true);
		scrollPane.setMinimumSize(new Dimension(100, 200));
		contentPane.add(scrollPane, gbc_scrollPane);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setSize(new Dimension(500, 500));
        panel.setBorder(new EmptyBorder(0, 5, 0, 5));
        scrollPane.setViewportView(panel);
        /***************************** Em caso do vector não ter veiculo ***********************************/
		JTextPane txtpnVeiculos = new JTextPane();
		txtpnVeiculos.setBackground(UIManager.getColor("Button.background"));
		txtpnVeiculos.setEditable(false);
		txtpnVeiculos.setText("Não há veiculos registrados");	
		panel.add(txtpnVeiculos);
		if (veiculos.isEmpty()) {
			txtpnVeiculos.setVisible(true);
		} else {
			txtpnVeiculos.setVisible(false);
		}
		/***************************** Esvazia e preenche copia do vector veiculos***********************************/
		v1.clear();
		conferePeso();
		/***************************** Ordena a copia do vector veiculos de acordo com o filtro***********************************/
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox.getSelectedItem().toString() == "+ rápido") {
					maisRapido();
				} else if (comboBox.getSelectedItem().toString() == "- custo") {
					menosCusto();
				} else if (comboBox.getSelectedItem().toString() == "Custo Beneficio") {
					
				}
			}
		});
		
		/***************************** Inicio da toolBar para os veiculos***********************************/
		for (int i = 0; i< v1.size(); i++) {
			JToolBar toolBar_2 = new JToolBar();
			toolBar_2.setFloatable(false);
			panel.add(toolBar_2);
//			/***************************** Inicio da toolBar para os veiculos***********************************/
			JTextPane txtpnTipoV = new JTextPane();
			txtpnTipoV.setBackground(UIManager.getColor("Button.background"));
			txtpnTipoV.setEditable(false);
			txtpnTipoV.setText(v1.elementAt(i).getTipo());
			toolBar_2.add(txtpnTipoV);
				
			JTextPane txtpnCombustivelV = new JTextPane();
			txtpnCombustivelV.setBackground(UIManager.getColor("Button.background"));
			txtpnCombustivelV.setEditable(false);
			txtpnCombustivelV.setText(v1.elementAt(i).getCombustivel());
			toolBar_2.add(txtpnCombustivelV);
			
			JTextPane txtpnTempo = new JTextPane();
			txtpnTempo.setBackground(UIManager.getColor("Button.background"));
			txtpnTempo.setEditable(false);
			txtpnTempo.setText(Double.toString(v1.elementAt(i).getTempo()) + " horas");
			txtpnTempo.setVisible(false);
			toolBar_2.add(txtpnTempo);
			
			JTextPane txtpnCusto = new JTextPane();
			txtpnCusto.setBackground(UIManager.getColor("Button.background"));
			txtpnCusto.setEditable(false);
			txtpnCusto.setText(Double.toString(v1.elementAt(i).getCusto()) + " reais");
			txtpnCusto.setVisible(false);
			toolBar_2.add(txtpnCusto);
			
			JTextPane txtpnRendimento = new JTextPane();
			txtpnRendimento.setBackground(UIManager.getColor("Button.background"));
			txtpnRendimento.setEditable(false);
			txtpnRendimento.setText(Double.toString(v1.elementAt(i).getRendimento()) + " km/l");
			txtpnRendimento.setVisible(false);
			toolBar_2.add(txtpnRendimento);
				
			JTextPane txtpnStatusV = new JTextPane();
			txtpnStatusV.setBackground(UIManager.getColor("Button.background"));
			txtpnStatusV.setEditable(false);
			txtpnStatusV.setText(v1.elementAt(i).getStatus());
			toolBar_2.add(txtpnStatusV);
	
			/***************************** Caso necessario terei o indice ***********************************/
			int j = i;
			v1.elementAt(i).setIndice(j);
			/***************************** Botão de informação em cada veiculo ***********************************/
			JButton btnInfo = new JButton("i");
			btnInfo.setToolTipText("Informações");
			toolBar_2.add(btnInfo);
			btnInfo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					contentPane.setVisible(false);
					Informacoes(v1.elementAt(j));
				}
			});
			/***************************** Botão de selecionar para viagem em cada veiculo ***********************************/
			JButton btnSeleciona = new JButton("Selecionar");
			btnSeleciona.setEnabled(false);
			toolBar_2.add(btnSeleciona);
			btnSeleciona.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					v1.elementAt(j).setStatus("Em viagem");
					contentPane.setVisible(false);
					Inicial();
				}
			});
			
			/***************************** Botão Nova Viagem ***********************************/
			btnNovaViagem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {  
				    	try {
				    		setPeso(Float.parseFloat(textPeso.getText()));
				    		setDistancia(Float.parseFloat(textDistancia.getText()));
				    		setPrazo(Float.parseFloat(textPrazo.getText()));
				    		comboBox.setEnabled(true); 		    		
		
							v1.elementAt(j).setTempo(getDistancia());
				    		v1.elementAt(j).setCusto(getPeso(), getDistancia());
				    		v1.elementAt(j).setRendimento(getPeso());
				    		
				    		contentPane.repaint();
				    		btnSeleciona.setEnabled(true);
				    		txtpnTempo.setVisible(true);
				    		txtpnCusto.setVisible(true);
				    		txtpnRendimento.setVisible(true);
				    	} catch (NumberFormatException ex)	{	
				    		JOptionPane.showMessageDialog(null, "Insira apenas números. Use ponto, não virgula para decimais");
				    	} // END TRY		    
				}
			});
			
		} //END FOR
}
	/***************************** FUNÇÂO PARA CRIAR E ADICIONAR NOVOS VEICULOS E VECTOR ***********************************/	
	//cria um novo veiculo
	private void NovoVeiculo() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JButton btnVoltar = new JButton("Voltar");
		GridBagConstraints gbc_btnVoltar = new GridBagConstraints();
		gbc_btnVoltar.insets = new Insets(0, 0, 5, 5);
		gbc_btnVoltar.gridx = 0;
		gbc_btnVoltar.gridy = 0;
		contentPane.add(btnVoltar, gbc_btnVoltar);
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  
				contentPane.setVisible(false);
				Inicial();			    
			}
		});
		
		JTextPane txtpnVeiculo = new JTextPane();
		txtpnVeiculo.setBackground(UIManager.getColor("Button.background"));
		txtpnVeiculo.setText("Veiculo");
		GridBagConstraints gbc_txtpnVeiculo = new GridBagConstraints();
		gbc_txtpnVeiculo.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnVeiculo.gridx = 0;
		gbc_txtpnVeiculo.gridy = 3;
		contentPane.add(txtpnVeiculo, gbc_txtpnVeiculo);
		
		String[] a = {" ","Carreta","Van","Carro","Moto"};
		JComboBox<String> comboBox = new JComboBox<String>(a);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.gridwidth = 3;
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 2;
		gbc_comboBox.gridy = 3;
		contentPane.add(comboBox, gbc_comboBox);	
		
		JTextPane txtpnCombustivel = new JTextPane();
		txtpnCombustivel.setBackground(UIManager.getColor("Button.background"));
		txtpnCombustivel.setEditable(false);
		txtpnCombustivel.setText("Combustivel");
		GridBagConstraints gbc_txtpnCombustivel = new GridBagConstraints();
		gbc_txtpnCombustivel.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnCombustivel.gridx = 0;
		gbc_txtpnCombustivel.gridy = 5;
		contentPane.add(txtpnCombustivel, gbc_txtpnCombustivel);
		
		JRadioButton rdbtnDiesel = new JRadioButton("Diesel");
		GridBagConstraints gbc_rdbtnDiesel = new GridBagConstraints();
		gbc_rdbtnDiesel.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnDiesel.gridx = 2;
		gbc_rdbtnDiesel.gridy = 5;
		contentPane.add(rdbtnDiesel, gbc_rdbtnDiesel);
		
		JRadioButton rdbtnAlcool = new JRadioButton("Alcool");
		GridBagConstraints gbc_rdbtnAlcool = new GridBagConstraints();
		gbc_rdbtnAlcool.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnAlcool.gridx = 3;
		gbc_rdbtnAlcool.gridy = 5;
		contentPane.add(rdbtnAlcool, gbc_rdbtnAlcool);
		
		JRadioButton rdbtnGasolina = new JRadioButton("Gasolina");
		GridBagConstraints gbc_rdbtnGasolina = new GridBagConstraints();
		gbc_rdbtnGasolina.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnGasolina.gridx = 4;
		gbc_rdbtnGasolina.gridy = 5;
		contentPane.add(rdbtnGasolina, gbc_rdbtnGasolina);
		
		ButtonGroup bgroup = new ButtonGroup();
        bgroup.add(rdbtnGasolina);
        bgroup.add(rdbtnAlcool);
        bgroup.add(rdbtnDiesel);
        /***************************** Validação dos RADIO BUTTON ***********************************/
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox.getSelectedItem().toString() == "Carro" || comboBox.getSelectedItem().toString() == "Moto") {
					rdbtnDiesel.setEnabled(false);
					rdbtnAlcool.setEnabled(true);
					rdbtnGasolina.setEnabled(true);
				} else if (comboBox.getSelectedItem().toString() == "Carreta" || comboBox.getSelectedItem().toString() == "Van") {
					rdbtnAlcool.setEnabled(false);
					rdbtnGasolina.setEnabled(false);
					rdbtnDiesel.setEnabled(true);
				} else {
					rdbtnDiesel.setEnabled(false);
					rdbtnAlcool.setEnabled(false);
					rdbtnGasolina.setEnabled(false);
				}
				bgroup.clearSelection();
			}
		});
		 /***************************** Validação das informações do veiculo ***********************************/
		JButton btnConfirma = new JButton("Confirma");
		GridBagConstraints gbc_btnConfirma = new GridBagConstraints();
		gbc_btnConfirma.insets = new Insets(0, 0, 0, 5);
		gbc_btnConfirma.gridx = 4;
		gbc_btnConfirma.gridy = 6;
		contentPane.add(btnConfirma, gbc_btnConfirma);
		btnConfirma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnDiesel.isSelected() || rdbtnAlcool.isSelected() || rdbtnGasolina.isSelected()) {
					String comb = null;
					if (rdbtnAlcool.isSelected()){
						comb = "alcool";
					} else if (rdbtnGasolina.isSelected()) {
						comb = "gasolina";
					}
					
					switch (comboBox.getSelectedItem().toString()) {
					case "Carreta":
						veiculos.add(new Carreta());
						break; 
					case "Van":
						veiculos.add(new Van());
						break; 
					case "Carro":
						veiculos.add(new Carro());
						veiculos.lastElement().setCombustivel(comb);
						break; 
					case "Moto":
						veiculos.add(new Moto());
						veiculos.lastElement().setCombustivel(comb);
						break; 
					case " ":
						JOptionPane.showMessageDialog(null, "Adicione um veiculo valido");
						break;
					}
					try {
						veiculos.lastElement().setIndice(veiculos.size());
					} catch (Exception e ) {
						JOptionPane.showMessageDialog(null, "Adicione um veiculo valido");
					}
			
					if (comboBox.getSelectedItem().toString() != " ") {
						JOptionPane.showMessageDialog(null, "Veiculo adicionado");
						contentPane.setVisible(false);
						Inicial();
					}
				}
			}
		});
	}

	//Apresenta as informações do veiculo selecionado
	/***************************** FUNÇÂO PARA VER E MODIFICAR INFORMAÇÕES EM CADA VEICULO ***********************************/
	private void Informacoes(Veiculo v) {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{180, 75, 0, 0};
		gbl_contentPane.rowHeights = new int[]{25, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JButton btnVoltar = new JButton("Voltar");
		GridBagConstraints gbc_btnVoltar = new GridBagConstraints();
		gbc_btnVoltar.insets = new Insets(0, 0, 5, 5);
		gbc_btnVoltar.gridx = 0;
		gbc_btnVoltar.gridy = 0;
		contentPane.add(btnVoltar, gbc_btnVoltar);
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  
				contentPane.setVisible(false);
				Inicial();			    
			}
		});
		
		JButton btnDeletar = new JButton("Deletar");
		GridBagConstraints gbc_btnDeletar = new GridBagConstraints();
		gbc_btnDeletar.insets = new Insets(0, 0, 5, 5);
		gbc_btnDeletar.gridx = 1;
		gbc_btnDeletar.gridy = 0;
		contentPane.add(btnDeletar, gbc_btnDeletar);
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  
				veiculos.removeElementAt(v.getIndice());
				Inicial();
			}
		});
		
		JTextPane txtpnTipo = new JTextPane();
		txtpnTipo.setEditable(false);
		txtpnTipo.setBackground(UIManager.getColor("Button.background"));
		txtpnTipo.setText("Tipo");
		GridBagConstraints gbc_txtpnTipo = new GridBagConstraints();
		gbc_txtpnTipo.anchor = GridBagConstraints.NORTH;
		gbc_txtpnTipo.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnTipo.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtpnTipo.gridx = 0;
		gbc_txtpnTipo.gridy = 1;
		contentPane.add(txtpnTipo, gbc_txtpnTipo);
		
		JTextPane txtpnT = new JTextPane();
		txtpnT.setBackground(UIManager.getColor("Button.background"));
		txtpnT.setEditable(false);
		txtpnT.setText(v.getTipo());
		GridBagConstraints gbc_txtpnT = new GridBagConstraints();
		gbc_txtpnT.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnT.fill = GridBagConstraints.BOTH;
		gbc_txtpnT.gridx = 1;
		gbc_txtpnT.gridy = 1;
		contentPane.add(txtpnT, gbc_txtpnT);
		
		JTextPane txtpnCombustivel = new JTextPane();
		txtpnCombustivel.setEditable(false);
		txtpnCombustivel.setBackground(UIManager.getColor("Button.background"));
		txtpnCombustivel.setText("Combustivel");
		GridBagConstraints gbc_txtpnCombustivel = new GridBagConstraints();
		gbc_txtpnCombustivel.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnCombustivel.fill = GridBagConstraints.BOTH;
		gbc_txtpnCombustivel.gridx = 0;
		gbc_txtpnCombustivel.gridy = 2;
		contentPane.add(txtpnCombustivel, gbc_txtpnCombustivel);
		
		JTextPane txtpnG = new JTextPane();
		txtpnG.setEditable(false);
		txtpnG.setBackground(UIManager.getColor("Button.background"));
		txtpnG.setText(v.getCombustivel());
		GridBagConstraints gbc_txtpnG = new GridBagConstraints();
		gbc_txtpnG.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnG.fill = GridBagConstraints.BOTH;
		gbc_txtpnG.gridx = 1;
		gbc_txtpnG.gridy = 2;
		contentPane.add(txtpnG, gbc_txtpnG);
		
		JButton btnM = new JButton("M");
		GridBagConstraints gbc_btnM = new GridBagConstraints();
		gbc_btnM.insets = new Insets(0, 0, 5, 0);
		gbc_btnM.gridx = 2;
		gbc_btnM.gridy = 2;
		contentPane.add(btnM, gbc_btnM);
		btnM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  
				if (v.getCombustivel() == "alcool") {
					v.setCombustivel("gasolina");
				} else if (v.getCombustivel() == "gasolina") {
					v.setCombustivel("alcool");
				}
				contentPane.setVisible(false);
				Informacoes(v);
			}
		});
		if (v.getCombustivel() == "diesel") {
			btnM.setEnabled(false);
		} else {
			btnM.setEnabled(true);
		}
		
		JTextPane txtpnPreco = new JTextPane();
		txtpnPreco.setEditable(false);
		txtpnPreco.setBackground(UIManager.getColor("Button.background"));
		txtpnPreco.setText("Preço da Gasolina");
		GridBagConstraints gbc_txtpnPreco = new GridBagConstraints();
		gbc_txtpnPreco.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnPreco.fill = GridBagConstraints.BOTH;
		gbc_txtpnPreco.gridx = 0;
		gbc_txtpnPreco.gridy = 3;
		contentPane.add(txtpnPreco, gbc_txtpnPreco);
		
		JTextPane txtpnPG = new JTextPane();
		txtpnPG.setText(Double.toString(v.getValorCombustivel()) + " preço por litro");
		txtpnPG.setEditable(false);
		txtpnPG.setBackground(UIManager.getColor("Button.background"));
		GridBagConstraints gbc_txtpnPG = new GridBagConstraints();
		gbc_txtpnPG.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnPG.fill = GridBagConstraints.BOTH;
		gbc_txtpnPG.gridx = 1;
		gbc_txtpnPG.gridy = 3;
		contentPane.add(txtpnPG, gbc_txtpnPG);
		
		JTextPane txtpnRendimento = new JTextPane();
		txtpnRendimento.setBackground(UIManager.getColor("Button.background"));
		txtpnRendimento.setEditable(false);
		txtpnRendimento.setText("Rendimento");
		GridBagConstraints gbc_txtpnRendimento = new GridBagConstraints();
		gbc_txtpnRendimento.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnRendimento.fill = GridBagConstraints.BOTH;
		gbc_txtpnRendimento.gridx = 0;
		gbc_txtpnRendimento.gridy = 4;
		contentPane.add(txtpnRendimento, gbc_txtpnRendimento);
		
		JTextPane txtpnR = new JTextPane();
		txtpnR.setText(Double.toString(v.getDefaultRendimento())+ " km/l");
		txtpnR.setEditable(false);
		txtpnR.setBackground(UIManager.getColor("Button.background"));
		GridBagConstraints gbc_txtpnR;
		gbc_txtpnR = new GridBagConstraints();
		gbc_txtpnR.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnR.fill = GridBagConstraints.BOTH;
		gbc_txtpnR.gridx = 1;
		gbc_txtpnR.gridy = 4;
		contentPane.add(txtpnR, gbc_txtpnR);
		
		JTextPane txtpnCapacidadeMxima = new JTextPane();
		txtpnCapacidadeMxima.setBackground(UIManager.getColor("Button.background"));
		txtpnCapacidadeMxima.setText("Capacidade Máxima");
		GridBagConstraints gbc_txtpnCapacidadeMxima = new GridBagConstraints();
		gbc_txtpnCapacidadeMxima.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnCapacidadeMxima.fill = GridBagConstraints.BOTH;
		gbc_txtpnCapacidadeMxima.gridx = 0;
		gbc_txtpnCapacidadeMxima.gridy = 5;
		contentPane.add(txtpnCapacidadeMxima, gbc_txtpnCapacidadeMxima);
		
		JTextPane txtpnC = new JTextPane();
		txtpnC.setBackground(UIManager.getColor("Button.background"));
		txtpnC.setEditable(false);
		txtpnC.setText(Double.toString(v.getCargaMaxima())+" kg");
		GridBagConstraints gbc_txtpnC = new GridBagConstraints();
		gbc_txtpnC.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnC.fill = GridBagConstraints.BOTH;
		gbc_txtpnC.gridx = 1;
		gbc_txtpnC.gridy = 5;
		contentPane.add(txtpnC, gbc_txtpnC);
		
		JTextPane txtpnVelocidade = new JTextPane();
		txtpnVelocidade.setBackground(UIManager.getColor("Button.background"));
		txtpnVelocidade.setText("Velocidade");
		GridBagConstraints gbc_txtpnVelocidade = new GridBagConstraints();
		gbc_txtpnVelocidade.insets = new Insets(0, 0, 0, 5);
		gbc_txtpnVelocidade.fill = GridBagConstraints.BOTH;
		gbc_txtpnVelocidade.gridx = 0;
		gbc_txtpnVelocidade.gridy = 6;
		contentPane.add(txtpnVelocidade, gbc_txtpnVelocidade);
		
		JTextPane txtpnV = new JTextPane();
		txtpnV.setBackground(UIManager.getColor("Button.background"));
		txtpnV.setEditable(false);
		txtpnV.setText(v.getVelocidade() + " k/h");
		GridBagConstraints gbc_txtpnV = new GridBagConstraints();
		gbc_txtpnV.insets = new Insets(0, 0, 0, 5);
		gbc_txtpnV.fill = GridBagConstraints.BOTH;
		gbc_txtpnV.gridx = 1;
		gbc_txtpnV.gridy = 6;
		contentPane.add(txtpnV, gbc_txtpnV); 
		
		JTextPane txtpnStatus = new JTextPane();
		txtpnStatus.setBackground(UIManager.getColor("Button.background"));
		txtpnStatus.setText("Status");
		GridBagConstraints gbc_txtpnStatus = new GridBagConstraints();
		gbc_txtpnStatus.insets = new Insets(0, 0, 0, 5);
		gbc_txtpnStatus.fill = GridBagConstraints.BOTH;
		gbc_txtpnStatus.gridx = 0;
		gbc_txtpnStatus.gridy = 7;
		contentPane.add(txtpnStatus, gbc_txtpnStatus);
		
		JTextPane txtpnS = new JTextPane();
		txtpnS.setBackground(UIManager.getColor("Button.background"));
		txtpnS.setEditable(false);
		txtpnS.setText(v.getStatus());
		GridBagConstraints gbc_txtpnS = new GridBagConstraints();
		gbc_txtpnS.insets = new Insets(0, 0, 0, 5);
		gbc_txtpnS.fill = GridBagConstraints.BOTH;
		gbc_txtpnS.gridx = 1;
		gbc_txtpnS.gridy = 7;
		contentPane.add(txtpnS, gbc_txtpnS);
		
		JButton btnM_1 = new JButton("M");
		GridBagConstraints gbc_btnM_1 = new GridBagConstraints();
		gbc_btnM_1.gridx = 2;
		gbc_btnM_1.gridy = 7;
		contentPane.add(btnM_1, gbc_btnM_1);
		btnM_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  
				v.setStatus("Disponivel");
				contentPane.setVisible(false);
				Informacoes(v);
			}
		});
		if (v.getStatus() == "Disponivel") {
			btnM_1.setEnabled(false);	
		} else {
			btnM_1.setEnabled(true);	
		}
		
	}
	
	//Reordena o v1 para a ordem do mais rapido
	public void maisRapido() {
//		for (int i=0; i<v1.size(); i++) {
//			System.out.println(v1.size());
//			
//			for (int j = 0; j < v1.size(); j++){
//                if (v1.elementAt(i).getVelocidade() < v1.elementAt(j).getVelocidade()){
//                	System.out.println(v1.elementAt(i).getVelocidade());
//                	System.out.println(v1.elementAt(j).getVelocidade());
//                	Veiculo vei;
//                	
//                	if (v1.elementAt(i).getTipo() == "Carreta") {
//                		vei = new Carreta();
//                	} else if (v1.elementAt(i).getTipo() == "Van") {
//                		vei = new Van();
//                	} else if (v1.elementAt(i).getTipo() == "Carro") {
//                		vei = new Carro();
//                	} else {
//                		vei = new Moto();
//                	}
//                	
//                	vei.equals(v1.elementAt(j));
//                	System.out.println(vei.getVelocidade());
//                	v1.elementAt(i)
//                	System.out.println(v1.elementAt(i).getVelocidade());
//                	System.out.println(v1.elementAt(j).getVelocidade());
//                } //END IF 
//            } //ENF FOR INT J
//		} //END FOR INT I	
	} // END MAIS RAPIDO
	
	//Reordena o v1 para a ordem do mais barato
	public void menosCusto() {
//		for (int i=0; i<v1.size(); i++) {
//			for (int j = 0; j < v1.size(); j++){
//				v1.elementAt(j).setCusto(peso, distancia);
//                if (v1.elementAt(i).getCusto() < v1.elementAt(j).getCusto()){
//                	//aqui acontece a troca, do maior cara  vaia para a direita e o menor para a esquerda
//                	Veiculo aux = v1.elementAt(i);
//                	v1.elementAt(i).equals(v1.elementAt(j)); 
//                	v1.elementAt(j).equals(aux);
//                } //END IF 
//            } //ENF FOR INT J
//		} //END FOR INT I
	}

	//Cria vector v1 que contem apenas veiculos que conseguem carregar a carga
	public void conferePeso() {
		for (int i=0; i<veiculos.size(); i++) {
			if (veiculos.elementAt(i).getCargaMaxima() >= getPeso()) {
				v1.add(veiculos.elementAt(i));
			}
		}
	}
	
	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public double getPrazo() {
		return prazo;
	}

	public void setPrazo(double prazo) {
		this.prazo = prazo;
	}
}