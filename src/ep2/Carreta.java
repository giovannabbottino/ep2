package ep2;

public class Carreta extends Veiculo {

	public Carreta() {
		this.setCombustivel("diesel");
		this.setDefaultRendimento(8.0);
		this.setCargaMaxima(30000);
		this.setVelocidade(60);
		this.setDiminuiRendimento(0.0002);
		this.setTipo("Carreta");
		this.setStatus("Disponivel");
	}

}
