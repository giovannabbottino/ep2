package ep2;

public class Van extends Veiculo {

	public Van() {
		this.setCombustivel("diesel");
		this.setDefaultRendimento(10.0);
		this.setCargaMaxima(3500);
		this.setVelocidade(80);
		this.setDiminuiRendimento(0.001);
		this.setTipo("Van");
		this.setStatus("Disponivel");
	}

}
