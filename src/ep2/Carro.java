package ep2;

public class Carro extends Veiculo {

	public Carro() {
		this.setCombustivel("gasolina");
		this.setCargaMaxima(360);
		this.setVelocidade(100);
		this.setTipo("Carro");
		this.setStatus("Disponivel");
	}
	
	public void setCombustivel(String gas) {
		switch (gas) {
        case "alcool":
            this.setDiminuiRendimento(0.0231);
            this.setDefaultRendimento(12);
            this.setValorCombustivel(3.499);
            break;
        case "gasolina":
            this.setDefaultRendimento(14);
            this.setDiminuiRendimento(0.025);
            this.setValorCombustivel(4.449);
            break;
        default:
             System.out.println("Este não é um combustivel válido!");
     }
		this.combustivel = gas;
		
	}
}
