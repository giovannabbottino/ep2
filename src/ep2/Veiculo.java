package ep2;

public abstract class Veiculo {
	protected String combustivel;
	protected String status;
	protected String tipo;
	private double defaultRendimento;
	private double cargaMaxima; 
	private int velocidade; 
	private double diminuiRendimento;
	private double valorCombustivel;
	private int indice;
	private double tempo;
	private double custo;
	private double rendimento;

	public String getCombustivel() {
		return combustivel;
	}
	public void setCombustivel(String combustivel) {
		switch (combustivel) {
        case "alcool":
            setValorCombustivel(3.499);
            break;
        case "gasolina":
            setValorCombustivel(4.449);
            break;
        case "diesel":
        	setValorCombustivel(3.869);
        	break;
        default:
             System.out.println("Este não é um combustivel válido!");
     }
		this.combustivel = combustivel;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	//Km/L
	public double getRendimento() {
		return rendimento;
	}
	public void setRendimento(double peso) {
		double rendimentoReduzido = peso * this.getDiminuiRendimento();
		double rendimento = this.getDefaultRendimento() - rendimentoReduzido;
		this.rendimento = rendimento;
	}	
	
	//Kg
	public double getCargaMaxima() {
		return cargaMaxima;
	}
	public void setCargaMaxima(double cargaMaxima) {
		this.cargaMaxima = cargaMaxima;
	}
	
	//Km/h
	public int getVelocidade() {
		return velocidade;
	}
	public void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}
	
	//a cada kg é reduzido km/l
	public double getDefaultRendimento() {
		return defaultRendimento;
	}
	public void setDefaultRendimento(double defaultRendimento) {
		this.defaultRendimento = defaultRendimento;
	}

	//por litro
	public double getValorCombustivel() {
		return valorCombustivel;
	}
	public void setValorCombustivel(double d) {
		this.valorCombustivel = d;
	}
	
	public int getIndice() {
		return indice;
	}
	public void setIndice(int indice) {
		this.indice = indice;
	}

	public double getTempo() {
		return tempo;
	}
	public void setTempo(double distancia) {
		double tempo = (distancia / getVelocidade()) * 60; 
		this.tempo = tempo;
	}
	
	public double getCusto() {
		return custo;
	}
	public void setCusto(double peso, double distancia) {
		double custo = 0;
		setRendimento(peso);
		custo = (distancia / getRendimento()) * getValorCombustivel();		
		this.custo = custo;
	}
	
	public double getDiminuiRendimento() {
		return diminuiRendimento;
	}
	public void setDiminuiRendimento(double diminuiRendimento) {
		this.diminuiRendimento = diminuiRendimento;
	}
}
