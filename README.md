# Giovanna Borges Bottino
## Matricula: 17/0011267 
### EP2 - OO 2019.1 (UnB - Gama)

Este projeto consiste em criar sistema em Java, onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos no [README](https://gitlab.com/oofga/eps/eps_2019_1/ep2) do repositório.

## Instruções de execução

Para compilar esse projeto você deve primeiro clonar o git
```
$ https://gitlab.com/Giobbottino/ep2.git
```

Após isso compile o programa em uma IDE de Java, de preferência o [Eclipse](https://www.eclipse.org/downloads/)
